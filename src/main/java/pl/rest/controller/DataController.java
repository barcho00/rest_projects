package pl.rest.controller;

import com.google.gson.Gson;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;
import pl.rest.RestApplication;
import pl.rest.model.Logged_ip;
import pl.rest.model.ServiceUsageLog;
import pl.rest.service.Logged_IpService;
import pl.rest.service.ToLogRepoService;


import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Optional;


@RestController
@RequiredArgsConstructor
public class DataController {

    private final Logged_IpService logged_ipService;
    private final ToLogRepoService toLogRepoService;

    Logger logger = LoggerFactory.getLogger(RestApplication.class);



    @GetMapping("/hi")
    public String hi(HttpServletRequest request){

        String ip = request.getRemoteAddr();
        Logged_ip logged_ip = new Logged_ip();
        logged_ip.setA_ip(ip);
        logged_ipService.save(logged_ip);

        //Map<String,String[]> parameterList;
        var parameterList = request.getHeaderNames().asIterator();
        var parameterListAll = request.getHeader("user-agent"); //header info
        String jsonString = new Gson().toJson(parameterListAll);

        int i = 0;
        while (parameterList.hasNext()==true){
            String log = parameterList.next();
            ++i;

            var parValue = new Gson().toJson(request.getHeader(log));
            logger.info(String.valueOf(i)+" "+ log+ " =  "+parValue);
        }
//
//
//        var kolejna_zmienna = request;
//        var jsonKolejna_zmienna = new Gson().toJson(kolejna_zmienna);
//        logger.info("kolejna zmienna: "+jsonKolejna_zmienna);
//
//        logger.info(String.valueOf(jsonString.length()));
        String S = "koniec";
//        toLog(request,jsonString);

        return S;
    }

    private void toLog(HttpServletRequest request,String jsonString){
        ServiceUsageLog serviceUsageLog = new ServiceUsageLog();
        serviceUsageLog.setA_requestType(jsonString);
        serviceUsageLog.setB_requestBody(request.toString());
//        serviceUsageLog.setC_responseType(response.getContentType().toString());
//        serviceUsageLog.setC_responseBody(resBody.getBody().toString());
        toLogRepoService.saveLog(serviceUsageLog);

    }

    @GetMapping("/countLog")
    public long countLog(){
        return  logged_ipService.countAllLog();
    }
//
//    public CustomResult getCustomResult(){
//        final String complexSql = "SELECT SUM(distance) as distanceSum....";
//        final CustomResult customResult = (CustomResult) jdbcTemplate.queryForObject(complexSql, new CustomResultRowMapper());
//
//        return customResult;
//    }


    @PostMapping("/getById")
    public Optional<Logged_ip> getById (Long S, HttpServletRequest request , HttpServletResponse response){

        Optional<Logged_ip> x = logged_ipService.getById(S);

        return x;
    }

    @GetMapping("/log")
    public List<Logged_ip> log(HttpServletRequest request){

        return  logged_ipService.getAll();
    }

    static final Logger log =
            LoggerFactory.getLogger(RestApplication.class);


//    @Before("within(pl.rest.*.(..)) && execution(public * *(..)) && @within(org.springframework.web.bind.annotation.RestController) && @annotation(org.springframework.web.bind.annotation.RequestMapping)")    public void endpointBefore(){
//        ServiceUsageLog serviceUsageLog = new ServiceUsageLog();
//        serviceUsageLog.setA_requestType("request.getContentType().toString()");
//        serviceUsageLog.setB_requestBody("reqBody.getBody().toString()");
//        serviceUsageLog.setC_responseType("response.getContentType().toString()");
//        serviceUsageLog.setC_responseBody("resBody.getBody().toString()");
//        toLogRepoService.saveLog(serviceUsageLog);
//
//    }

//    @Before("within(your.package.where.is.endpoint..*)")
//    public void endpointBefore(JoinPoint p) {
//        if (log.isTraceEnabled()) {
//            log.trace(p.getTarget().getClass().getSimpleName() + " " + p.getSignature().getName() + " START");
//            Object[] signatureArgs = p.getArgs();
//
//
//            ObjectMapper mapper = new ObjectMapper();
//            mapper.enable(SerializationFeature.INDENT_OUTPUT);
//            try {
//
//                if (signatureArgs[0] != null) {
//                    log.trace("\nRequest object: \n" + mapper.writeValueAsString(signatureArgs[0]));
//                }
//            } catch (JsonProcessingException e) {
//            }
//        }
//    }
//
//    here `@Before("within(your.package.where.is.endpoint..*)")` has the package path. All endpoints within this package will generate the log.

}
