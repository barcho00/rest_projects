package pl.rest.controller;

import lombok.RequiredArgsConstructor;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import pl.rest.model.ServiceUsageLog;
import pl.rest.service.ToLogRepoService;

@Aspect
@RequiredArgsConstructor
public class DoBefore {

    private final ToLogRepoService toLogRepoService;


    @Before("within(pl.rest.*.(..)) && execution(public * *(..)) && @within(org.springframework.web.bind.annotation.RestController) && @annotation(org.springframework.web.bind.annotation.RequestMapping)")
    public void endpointBefore() {
        ServiceUsageLog serviceUsageLog = new ServiceUsageLog();
        serviceUsageLog.setA_requestType("request.getContentType().toString()");
        serviceUsageLog.setB_requestBody("reqBody.getBody().toString()");
        serviceUsageLog.setC_responseType("response.getContentType().toString()");
        serviceUsageLog.setC_responseBody("resBody.getBody().toString()");
        toLogRepoService.saveLog(serviceUsageLog);
    }
}