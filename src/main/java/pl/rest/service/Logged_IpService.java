package pl.rest.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.yaml.snakeyaml.events.Event;
import pl.rest.model.Logged_ip;
import pl.rest.repository.LogRepo;

import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class Logged_IpService {
    private final LogRepo logRepo;


    public List <Logged_ip> getAll(){
        return logRepo.findAll();
    }

    public void save(Logged_ip logged_ip){

        logRepo.save(logged_ip);
    }
    public long countAllLog(){return logRepo.count();}

    public Optional<Logged_ip> getById(Long id) throws IllegalArgumentException {
        return logRepo.findById(id);}

}
