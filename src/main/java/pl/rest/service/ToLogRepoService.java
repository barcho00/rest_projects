package pl.rest.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import pl.rest.model.ServiceUsageLog;
import pl.rest.repository.ToLogRepo;


@Service
@RequiredArgsConstructor
public class ToLogRepoService {

    private final ToLogRepo toLogRepo;

    public void saveLog(ServiceUsageLog serviceUsageLog){
        toLogRepo.save(serviceUsageLog);
    }

}
