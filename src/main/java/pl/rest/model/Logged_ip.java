package pl.rest.model;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Getter
@Setter
public class Logged_ip {
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private Long a_id;

    @Column(name = "ip")
    private String a_ip;

    @CreationTimestamp
    @Column(name = "created")
    private LocalDateTime c_created;

    @UpdateTimestamp
    @Column(name = "updated")
    private LocalDateTime c_updated;

    }
