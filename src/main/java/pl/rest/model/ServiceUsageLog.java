package pl.rest.model;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Getter
@Setter
public class ServiceUsageLog {
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private int a_id;

    @Column(name = "requestType",columnDefinition = "varchar(1124818)")
    private String a_requestType;

    @Column(name = "requestBody",columnDefinition = "varchar(15000)")
    private String b_requestBody;

    @Column(name = "responseType",columnDefinition = "varchar(15000)")
    private String c_responseType;

    @Column(name = "responseBody",columnDefinition = "varchar(15000)")
    private String c_responseBody;

    @CreationTimestamp
    @Column(name = "created")
    private LocalDateTime c_created;
}
