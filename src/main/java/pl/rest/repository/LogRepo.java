package pl.rest.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.rest.model.Logged_ip;

@Repository
public interface LogRepo extends JpaRepository<Logged_ip,Long> {

}

